#!/bin/bash

#script relative path
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

#cek jika log.txt sudah dibuat
n=$(find -name 'log.txt' | wc -l)
if [ $n == 0 ]; then
touch log.txt
fi

#cek jika users/users.txt sudah dibuat
n=$(find -name 'users' | wc -l)
if [ $n == 0 ]; then
mkdir users
touch users/users.txt
else
n=$(find $script_path"/users/" -name 'users.txt' | wc -l)
if [ $n == 0 ]; then
touch users/users.txt
fi
fi

#path users.txt dan log.txt
users_path=$script_path"/users/users.txt"
log_path=$script_path"/log.txt" #

echo "Login"
read -p 'Username: ' username
read -sp 'Password: ' pass
echo ""

#validate login credentials
stat=$(awk -v var1="$username" -v var2="$pass" 'BEGIN { flag=0 }
{if (var1 == $1 && flag==0){
	flag=1;
	if(var2 == $2){print "success"}
	else{print "err"}
}}' $users_path)

if [ "$stat" == "success" ]; then
echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $username" >> $log_path
echo "Login success"
else
echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> $log_path
echo "Login failed"
fi
