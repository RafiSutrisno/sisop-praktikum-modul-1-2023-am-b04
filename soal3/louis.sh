#!/bin/bash

#script relative path
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

#cek jika log.txt sudah dibuat
n=$(find -name 'log.txt' | wc -l)
if [ $n == 0 ]; then
touch log.txt
fi

#cek jika users/users.txt sudah dibuat
n=$(find -name 'users' | wc -l)
if [ $n == 0 ]; then
mkdir users
touch users/users.txt
else
n=$(find $script_path"/users/" -name 'users.txt' | wc -l)
if [ $n == 0 ]; then
touch users/users.txt
fi
fi

#path users.txt dan log.txt
users_path=$script_path"/users/users.txt"
log_path=$script_path"/log.txt"

echo "Register new account"
read -p 'Username: ' username
read -sp 'Password: ' pass
echo ""
	
#username validation
stat=$(awk -v var1="$username" 'BEGIN { flag="false" } { if(var1 == $1 && flag == "false"){print "found"; flag="true"} }' $users_path)
if [ "$stat" == "found" ]; then
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> $log_path
echo "Error User already exists"
else

#cek validitas substring password 
stat=$(echo "$pass" | awk -v username="$username" 'BEGIN { flag="valid" } { if( /ernie/ || /chicken/ || $0 == username ) { flag="invalid" }} END { print flag }')
if [ "$stat" == "valid" ]; then

#cek validitas length, upper + lowercase, alfabet + num, & nonalphanumeric 
stat=$(echo "$pass" | awk 'BEGIN { flag="invalid" } { if( length($0) > 7 && /[a-z]/ && /[A-Z]/ && /[0-9]/ ) { flag="valid" }} END { print flag }')
if [[ "$stat" == "valid" && !("$pass" =~ [^a-zA-Z0-9]) ]]; then
echo "$username $pass" >> $users_path
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> $log_path
echo "Register success"
else
echo "Error Password invalid"
fi
else
echo "Error Password invalid"
fi
fi
