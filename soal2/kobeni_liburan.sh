#!/bin/bash

#Mendaftarkan cron bila belum terdaftar
spath=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )"/kobeni_liburan.sh"
crontab -l | grep -q 'kobeni_liburan.sh' && stat=1 || stat=0

if [ $stat == 0 ]; then
nexthour=$(date -d '+10 hour' +%H)
(crontab -l | grep -v 'kobeni_liburan.sh' ; echo "0 $nexthour * * * bash $spath" ; echo "59 23 * * * bash $spath" ) | crontab -
fi

path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
prefix='kumpulan_'
currenttime=$(date +%H%M)
n=1

 #Cari folder kumpulan_ yang paling baru
if [ $currenttime != 2359 ]; then 
 while [ -d $path/$prefix$n ]; do ((n++))
 done

 #Buat folder
 echo "Membuat $prefix$n..."
 mkdir $path/$prefix$n
 echo "$prefix$n telah dibuat"

 #Cari tahu jam, jika 0 maka diloop sekali 
 times=$(date +%H | bc)
 if [ $times == 0 ]; then
  times=1
 fi

 #Download file berdasarkan jam
 for((i=1;i<=times;i=i+1))
 do
  echo "Sedang mengambil perjalanan_$i.png..."
  wget -O $path/$prefix$n/perjalanan_$i.png https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Indonesia_map_with_name_of_islands.png/800px-Indonesia_map_with_name_of_islands.png
  echo "Selesai mengambil perjalanan_$i.png"
 done
fi

#Cek waktu, jika dirun pada jam 23:59 maka akan melakukan zip
if [ $currenttime == 2359 ]; then
  devil='devil_'
  d=1

  while [ `ls $path | grep $devil$d.zip` ]; do ((d++))
  done

  cd $path
  zip $devil$d -rm `find . -name "kumpulan_*" -print`
fi

#Update cron jika melakukan download
if [ $currenttime != 2359 ]; then
nexthour=$(date -d '+10 hour' +%H)
(crontab -l | grep -v 'kobeni_liburan.sh' ; echo "0 $nexthour * * * bash $spath" ; echo "59 23 * * * bash $spath" ) | crontab -
fi
