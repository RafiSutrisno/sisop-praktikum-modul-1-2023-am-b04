# Sisop-Praktikum-Modul-1-2023-AM-B04

## Anggota Kelompok B04:
+ Akbar Putra Asenti Priyanto (5025211004)
+ Muhammad Rafi Sutrisno (5025211167)
+ Muhammad Rafi Insan Fillah (5025211169)

# Soal 1

## Penjelasan Script
1.a. Menampilkan 5 Universitas dengan ranking tertinggi di Jepang. Hasil tersebut dapat diperoleh dengan 2 tahap:

	> Filter universitas dengan location code "JP" menggunakan awk delimiter simbol ',' pada kolom ketiga yang bernilai "JP" dari file csv

	> Hasil filter tersebut dijadikan input untuk mengambil hanya 5 baris teratas menggunakan head command

1.b. Mencari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a. Hal tersebut dapat diperoleh dengan 3 tahap:

	> Filter universitas dengan location code "JP" menggunakan awk sama seperti poin 1.1.

	> Hasil filter tersebut dijadikan input untuk mengurutkan baris berdasarkan fsr score menggunakan command sort options baris ke-9

	> Hasil sorting kemudian dijadikan input untuk mengambil hanya 5 baris teratas menggunakan head command

1.c. Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi. Hal tersebut dapat diperoleh mirip dengan poin 1.b. dengan 3 tahap:

	> Filter universitas dengan location code "JP" menggunakan awk sama seperti poin 1.b.

	> Hasil filter tersebut dijadikan input untuk mengurutkan baris berdasarkan ger rank menggunakan command sort options baris ke-20
    
	> Hasil sorting kemudian dijadikan input untuk mengambil hanya 10 baris teratas menggunakan head command

1.d. Mencari universitas tersebut dengan kata kunci "keren". Karena tidak dijelaskan secara eksplisit kata kunci tersebut ada pada kolom berapa maka dilakukan search pattern pada seluruh kolom pada csv file. Filter tersebut menggunakan awk dengan pattern yg dicari '/keren/' dengan input file csv.

## university_survey.sh

```
#!/bin/bash

#poin 1
echo "poin 1:"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'|head -n 5

#poin 2
echo "poin 2:"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'| sort -t',' -k 9,9 -n |head -n 5 

#poin 3
echo "poin 3"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'| sort -t ',' -k 20,20 -n| head -n 10

#poin 4
echo "poin 4:"
awk '/Keren/ {print}' '2023 QS World University Rankings.csv'

```

# Soal 2

## kobeni_liburan.sh

```
#!/bin/bash

#Mendaftarkan cron bila belum terdaftar
spath=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )"/kobeni_liburan.sh"
crontab -l | grep -q 'kobeni_liburan.sh' && stat=1 || stat=0

if [ $stat == 0 ]; then
nexthour=$(date -d '+10 hour' +%H)
(crontab -l | grep -v 'kobeni_liburan.sh' ; echo "0 $nexthour * * * bash $spath" ; echo "59 23 * * * bash $spath" ) | crontab -
fi

path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
prefix='kumpulan_'
currenttime=$(date +%H%M)
n=1

 #Cari folder kumpulan_ yang paling baru
if [ $currenttime != 2359 ]; then 
 while [ -d $path/$prefix$n ]; do ((n++))
 done

 #Buat folder
 echo "Membuat $prefix$n..."
 mkdir $path/$prefix$n
 echo "$prefix$n telah dibuat"

 #Cari tahu jam, jika 0 maka diloop sekali 
 times=$(date +%H | bc)
 if [ $times == 0 ]; then
  times=1
 fi

 #Download file berdasarkan jam
 for((i=1;i<=times;i=i+1))
 do
  echo "Sedang mengambil perjalanan_$i.png..."
  wget -O $path/$prefix$n/perjalanan_$i.png https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Indonesia_map_with_name_of_islands.png/800px-Indonesia_map_with_name_of_islands.png
  echo "Selesai mengambil perjalanan_$i.png"
 done
fi

#Cek waktu, jika dirun pada jam 23:59 maka akan melakukan zip
if [ $currenttime == 2359 ]; then
  devil='devil_'
  d=1

  while [ `ls $path | grep $devil$d.zip` ]; do ((d++))
  done

  cd $path
  zip $devil$d -rm `find . -name "kumpulan_*" -print`
fi

#Update cron jika melakukan download
if [ $currenttime != 2359 ]; then
nexthour=$(date -d '+10 hour' +%H)
(crontab -l | grep -v 'kobeni_liburan.sh' ; echo "0 $nexthour * * * bash $spath" ; echo "59 23 * * * bash $spath" ) | crontab -
fi
```
## Penjelasan Script
Tahap pertama pada script ini adalah menyimpan path script ke dalam variabel **spath**. Selanjutnya, script akan melakukan pengecekan apakah telah membuat cron job yang dibutuhkan. Apabila belum, maka akan dibuat dua buah cron job, yaitu cron job untuk melakukan `bash` script 10 jam setelah jam sekarang dan cron job untuk melakukan `bash` script setiap hari pada jam 23:59 (Untuk melakukan `zip`).

Langkah selanjutnya adalah proses membuat folder *kumpulan_* baru dan download file *perjalanan_* sebanyak jam sekarang. Pertama-tama, dilakukan pengecekan waktu sekarang agar dapat membedakan proses `zip` dengan proses pembuatan folder. Kemudian dilakukan looping dengan variabel **n** yang diincrement untuk mencari folder *kumpulan_* berapa yang paling akhir dibuat. Setelah itu, dilakukan command `mkdir` untuk membuat folder tersebut di direktori script yang disimpan dalam variabel **path**.

Selanjutnya dilakukan pengecekan jam sekarang (disimpan di variabel **times**), karena telah ditentukan di soal bahwa pada pukul 00 dilakukan pengulangan sebanyak satu kali. Variabel **times** kemudian digunakan sebagai batas looping untuk pengunduhan file dengan menggunakan command `wget` dengan outputnya dikeluarkan di address *path/kumpulan_n/perjalanan_i.jpg*.

Langkah selanjutnya adalah melakukan pengecekan apakah akan diadakan `zip`. Dalam script ditentukan bahwa pengezipan folder akan dilakukan pada jam 23:59 setiap harinya. Dengan bantuan command `ls $path | grep $devil$d.zip` kita dapat mencari file *devil.zip* yang terakhir dibuat. Setelah ditemukan nama file .zip yang diperlukan, kita melakukan `cd` ke direktori script dan melakukan command  `zip -rm` ke semua folder *kumpulan_* yang ada pada saat itu.

Setelah semua langkah selesai, dilakukan pengecekan apakah script dijalankan untuk melakukan download atau pengezipan. Apabila script dijalankan untuk melakukan download, maka waktu cron job pertama akan diupdate ke 10 jam setelah jam sekarang. Apabila script dijalankan untuk melakukan `zip`, maka tidak dilakukan apa-apa.

# Soal 3

## louis.sh
Script louis merupakan script untuk register akun ke dalam /users/users.txt.

Tahap pertama pada script ini adalah definisi path script dengan memanfaatkan command dirname. Selanjutnya pembuatan file users.txt di dalam users direktori pada direktori yang sama dengan script jika file tersebut belum ada. Lalu pembuatan log.txt pada direktori yang sama dengan script bila file tersebut belum ada.  

Tahap selanjutnya definisi path untuk file users.txt dan log.txt. Dilanjutkan dengan input username pada variabel username dan password pada variabel pass yang akan diregister dengan command read. Setelah mendapat username maka username tersebut akan dicari dalam users.txt menggunakan awk, bila ternyata ditemukan username yang sama maka akan message error register dan di-append ke log.txt.

Bila username tidak duplikat maka selanjutnya validasi password, lapis pertama validasi adalah pengecekan bila terdapat kata "ernie" "chicken" atau password sama dengan username menggunakan awk. Bila memenuhi syarat lapis pertama maka dilakukan pengecekan password panjang minimal 8 karakter; setidaknya terdapat 1 karakter uppercase, lowercase, dan numerik;. Bila memenuhi lapis kedua maka selanjutnya dilakukan pengecekan terakhir yaitu cek apakah password hanya merupakan karakter alphanumerik. Bila memenuhi syarat password maka set username dan password akan di-append ke users.txt dan append success register message ke log.txt.

```
#!/bin/bash

#script relative path
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

#cek jika log.txt sudah dibuat
n=$(find -name 'log.txt' | wc -l)
if [ $n == 0 ]; then
touch log.txt
fi

#cek jika users/users.txt sudah dibuat
n=$(find -name 'users' | wc -l)
if [ $n == 0 ]; then
mkdir users
touch users/users.txt
else
n=$(find $script_path"/users/" -name 'users.txt' | wc -l)
if [ $n == 0 ]; then
touch users/users.txt
fi
fi

#path users.txt dan log.txt
users_path=$script_path"/users/users.txt"
log_path=$script_path"/log.txt"

echo "Register new account"
read -p 'Username: ' username
read -sp 'Password: ' pass
echo ""
	
#username validation
stat=$(awk -v var1="$username" 'BEGIN { flag="false" } { if(var1 == $1 && flag == "false"){print "found"; flag="true"} }' $users_path)
if [ "$stat" == "found" ]; then
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> $log_path
echo "Error User already exists"
else

#cek validitas substring password 
stat=$(echo "$pass" | awk -v username="$username" 'BEGIN { flag="valid" } { if( /ernie/ || /chicken/ || $0 == username ) { flag="invalid" }} END { print flag }')
if [ "$stat" == "valid" ]; then

#cek validitas length, upper + lowercase, alfabet + num, & nonalphanumeric 
stat=$(echo "$pass" | awk 'BEGIN { flag="invalid" } { if( length($0) > 7 && /[a-z]/ && /[A-Z]/ && /[0-9]/ ) { flag="valid" }} END { print flag }')
if [[ "$stat" == "valid" && !("$pass" =~ [^a-zA-Z0-9]) ]]; then
echo "$username $pass" >> $users_path
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> $log_path
echo "Register success"
else
echo "Error Password invalid"
fi
else
echo "Error Password invalid"
fi
fi


```

## retep.sh

Script retep merupakan script untuk login dengan source user yang terdaftar pada users.txt. Tahap pertama pada script ini adalah definisi path script dengan memanfaatkan command dirname. Selanjutnya pembuatan file users.txt di dalam users direktori pada direktori yang sama dengan script jika file tersebut belum ada. Lalu pembuatan log.txt pada direktori yang sama dengan script bila file tersebut belum ada.  

Tahap selanjutnya definisi path untuk file users.txt dan log.txt. Dilanjutkan dengan input username pada variabel username dan password pada variabel pass yang akan login dengan command read. Setelah mendapat username maka username tersebut akan dicari dalam users.txt menggunakan awk, bila ternyata ditemukan username yang sama maka akan dicek variabel pass untuk username tersebut apakah sudah sesuai dengan password yang terdaftar atau tidak. Bila sesuai maka login sukses dan message success login akan di-append ke long.txt. Bila password salah maka message error login akan di-append ke log.txt.

Perlu digarisbawahi bahwa validasi syarat password hanya ada pada script louis.sh karena hanya set username dan password yang valid yang dapat terdaftar pada users.txt sehingga bila password yang diinputkan pada login tidak valid maka kondisi error pasti terjadi tanpa harus validasi password.

```
#!/bin/bash

#script relative path
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

#cek jika log.txt sudah dibuat
n=$(find -name 'log.txt' | wc -l)
if [ $n == 0 ]; then
touch log.txt
fi

#cek jika users/users.txt sudah dibuat
n=$(find -name 'users' | wc -l)
if [ $n == 0 ]; then
mkdir users
touch users/users.txt
else
n=$(find $script_path"/users/" -name 'users.txt' | wc -l)
if [ $n == 0 ]; then
touch users/users.txt
fi
fi

#path users.txt dan log.txt
users_path=$script_path"/users/users.txt"
log_path=$script_path"/log.txt" #

echo "Login"
read -p 'Username: ' username
read -sp 'Password: ' pass
echo ""

#validate login credentials
stat=$(awk -v var1="$username" -v var2="$pass" 'BEGIN { flag=0 }
{if (var1 == $1 && flag==0){
	flag=1;
	if(var2 == $2){print "success"}
	else{print "err"}
}}' $users_path)

if [ "$stat" == "success" ]; then
echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $username" >> $log_path
echo "Login success"
else
echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> $log_path
echo "Login failed"
fi


```

# Soal 4
## log_encrypt.sh
```
!/bin/bash

# Crontab untuk soal no 4
# 0 */2 * * * bash log_encrypt.sh

# Ambil waktu saat ini
now=$(date +"%H:%M %d:%m:%Y")

# Buat format nama file backup dengan waktu saat ini
filename="$now.txt"

# Baca isi file syslog
syslog="$(cat /var/log/syslog)"

# Enkripsi isi file  yang sesuai dengan jam saat ini
dual=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
hour=$(date +"%H")
newphrase=$(echo -e "\n$syslog" | tr "${dual:0:26}" "${dual:${hour}:26}")

# Simpan file backup dengan nama dan isi yang sudah dienkripsi dengan baris awal adalah jam enkripsi
hour+=$newphrase
echo "$hour" > "$filename"
```

## log_decrypt.sh
```
#!/bin/bash
# Membaca file apa yang ingin dideskripsi
read namafile

# Ambil info jam enkripsi dari baris pertama menggunakan awk
hour=$(awk 'NR==1{print}' "$namafile.txt")  

# Baca isi syslog
syslog="$(awk '{   
        if((n==0)){
          n++
        }else{
          print
          n++
        }
}' "$namafile.txt")"

# Deskripsi file ke bentuk semula
dual=zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba
newphrase=$(echo "$syslog" | tr "${dual:0:26}" "${dual:${hour}:26}")
echo "$newphrase"
```

## Penjelasan Script
Untuk file log_encrypt.sh cara kerjanya adalah pertama ambil waktu encrypt dengan date. Selanjutnya buat format nama file backup dengan waktu yang telah didapat. Selanjutnya baca isi file syslog dengan cat. Lalu enkripsi file sesuai dengan jam saat ini dengan cara pertama buat variabel dual dengan isi dari a-z sebanyak dua kali, agar jika huruf yang ditambah melebihi z masih bisa di enkripsi. Enkripsi dengan mereplace huruf dengan command tr. Command tr mentranslate character dari input dan menuliskan hasil ke output. Huruf ditambah dengan jam saat ini. Lalu simpan file backup dengan jam enkripsi dengan menyematkan jam enkripsi di bagian atas file untuk proses deskripsi.

Untuk file log_decrypt.sh cara kerjanya adalah pertama kita jalankan file lalu kita masukkan nama file yang ingin dideskripsi. Setelah itu ambil info jam enkripsi yang ada di baris pertama file yang ingin di deskripsi dengan menggunakan awk. Setelah itu baca isi syslog dengan menggunakan awk, dimana yang perlu dibaca adalah semua baris kecuali baris pertama dan masukkan ke variabel. Selanjutnya masukkan variabel tersebut ke dalam fungsi deskripsi. Fungsi deskripsi sama dengan fungsi enkripsi hanya saja dual nya kita balik dari z-a sebanyak dua kali. Lalu hasil deskripsi akan dimunculkan di terminal.
