#!/bin/bash
read namafile
hour=$(awk 'NR==1{print}' "$namafile.txt")

syslog="$(awk '{
	if((n==0)){
	  n++
	}else{
	  print
	  n++
	}
}' "$namafile.txt")"


dual=zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba
newphrase=$(echo "$syslog" | tr "${dual:0:26}" "${dual:${hour}:26}")

echo "$newphrase"
