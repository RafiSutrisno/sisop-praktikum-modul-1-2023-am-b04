#!/bin/bash

#crontab untuk soal no 4
# 0 */2 * * * bash log_encrypt.sh

# Ambil waktu saat ini
now=$(date +"%H:%M %d:%m:%Y")

# Buat format nama file backup dengan waktu saat ini
filename="$now.txt"

# Baca isi file syslog
syslog="$(cat /var/log/syslog)"

# Enkripsi isi file  yang sesuai dengan jam saat ini
dual=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
hour=$(date +"%H")
newphrase=$(echo -e "\n$syslog" | tr "${dual:0:26}" "${dual:${hour}:26}")

# Simpan file backup dengan nama dan isi yang sudah dienkripsi dengan baris awal adalah jam enkripsi
hour+=$newphrase
echo "$hour" > "$filename"


